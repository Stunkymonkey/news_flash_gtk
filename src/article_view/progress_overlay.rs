use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate};
use gtk4::{Box, ProgressBar, Widget};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_view_progress.ui")]
    pub struct ProgressOverlay {
        #[template_child]
        pub progress: TemplateChild<ProgressBar>,
    }

    impl Default for ProgressOverlay {
        fn default() -> Self {
            Self {
                progress: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ProgressOverlay {
        const NAME: &'static str = "ProgressOverlay";
        type ParentType = Box;
        type Type = super::ProgressOverlay;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ProgressOverlay {}

    impl WidgetImpl for ProgressOverlay {}

    impl BoxImpl for ProgressOverlay {}
}

glib::wrapper! {
    pub struct ProgressOverlay(ObjectSubclass<imp::ProgressOverlay>)
        @extends Widget, Box;
}

impl ProgressOverlay {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn set_percentage(&self, percentage: f64) {
        let imp = imp::ProgressOverlay::from_instance(self);
        let mut percentage = percentage;
        if percentage < 0.0 {
            percentage = 0.0;
        }
        if percentage > 1.0 {
            percentage = 1.0;
        }
        imp.progress.set_fraction(percentage);
    }

    pub fn reveal(&self, show: bool) {
        if show {
            self.show();
        } else {
            self.hide();
        }
    }
}
