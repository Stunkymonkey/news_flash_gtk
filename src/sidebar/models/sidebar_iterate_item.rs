use crate::sidebar::FeedListItemID;
use news_flash::models::TagID;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub enum SidebarIterateItem {
    SelectAll,
    FeedListSelectFirstItem,
    FeedListSelectLastItem,
    TagListSelectFirstItem,
    TagListSelectLastItem,
    SelectFeedListItem(Arc<FeedListItemID>),
    SelectTagList(Arc<TagID>),
    NothingSelected,
}
