use crate::sidebar::FeedListItemID;
use news_flash::models::TagID;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub enum SidebarSelection {
    All,
    FeedList(Arc<FeedListItemID>, String),
    Tag(Arc<TagID>, String),
}

impl SidebarSelection {
    pub fn from_feed_list_selection(selection: FeedListItemID, title: String) -> Self {
        SidebarSelection::FeedList(Arc::new(selection), title)
    }

    pub fn from_tag_list_selection(selection: Arc<TagID>, title: String) -> Self {
        SidebarSelection::Tag(selection, title)
    }
}

impl PartialEq for SidebarSelection {
    fn eq(&self, other: &SidebarSelection) -> bool {
        match self {
            SidebarSelection::All => match other {
                SidebarSelection::All => true,
                _ => false,
            },
            SidebarSelection::FeedList(self_id, _title) => match other {
                SidebarSelection::FeedList(other_id, _title) => self_id == other_id,
                _ => false,
            },
            SidebarSelection::Tag(self_id, _title) => match other {
                SidebarSelection::Tag(other_id, _title) => self_id == other_id,
                _ => false,
            },
        }
    }
}
